#!/usr/bin/python
# Copyright (c) 2018, Daniele Venzano
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
# implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""R notebook ZApp description generator."""

import json
import sys
import os

APP_NAME = 'rdatasci'
ZOE_APPLICATION_DESCRIPTION_VERSION = 3

options = {
    'memory_limit': {
        'value': 6 * (1024**3),
        'description': 'Memcached memory limit (bytes)'
    },
    'core_limit': {
        'value': 2,
        'description': 'Number of cores to allocate'
    }
}

REPOSITORY = os.getenv("REPOSITORY", default="zapps")
VERSION = os.getenv("VERSION", default="latest")

IMAGE = REPOSITORY + '/rdatasci:' + VERSION

def nb_service(memory_limit, core_limit, image):
    """
    :rtype: dict
    """
    mem_limit_mb = memory_limit / (1024**2)
    service = {
        'name': "r-notebook",
        'image': image,
        'monitor': True,
        'resources': {
            "memory": {
                "min": memory_limit,
                "max": memory_limit
            },
            "cores": {
                "min": core_limit,
                "max": core_limit
            }
        },
        'ports': [
            {
                "name": "Jupyter Notebook interface",
                "port_number": 8888,
                "protocol": "tcp",
                "url_template": "http://{ip_port}{proxy_path}",
                "proxy": True
            }
        ],
        'environment': [],
        'volumes': [],
        'command': '/usr/local/bin/start_notebook.sh',
        'total_count': 1,
        'essential_count': 1,
        'startup_order': 0,
        'replicas': 1
    }

    return service


if __name__ == '__main__':
    app = {
        'name': APP_NAME,
        'version': ZOE_APPLICATION_DESCRIPTION_VERSION,
        'will_end': False,
        'size': 512,
        'services': [
            nb_service(options["memory_limit"]["value"], options["core_limit"]["value"], IMAGE)
        ]
    }

    json.dump(app, open("rdatasci.json", "w"), sort_keys=True, indent=4)

    with open('images', 'w') as fp:
        fp.write(IMAGE + '\n')

    print("ZApp written")

