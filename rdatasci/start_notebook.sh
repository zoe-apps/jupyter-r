#!/bin/sh

/opt/conda/bin/jupyter lab --no-browser --NotebookApp.token="${NOTEBOOK_PASSWORD}" --allow-root --ip=0.0.0.0 --NotebookApp.base_url=${REVERSE_PROXY_PATH_8888}

